#include "inc/parx/json.hpp"
#include <type_traits>
#include <iterator>

parx::key parx::string_literals::operator""_key(char const* f, std::size_t s)
{
  return {f, f + s};
}

parx::json::json(char const* ptr) : json(std::string(ptr))
{
}

parx::json::json(std::initializer_list<std::pair<parx::key const, json>> init)
{
  auto& doc = get::document(*this);
  for (auto & [ k, v ] : init)
    doc.insert(document::value_type(std::string(k.first, k.last), std::move(v)));
}

parx::json::json(std::initializer_list<json> init) : json(array(init))
{
}

auto parx::json::size() const noexcept -> size_type
{
  auto const visitor = overload([](none) -> size_type { return 0; },
                                [](auto const& x) -> size_type {
                                  if
                                    constexpr(has_size(x)) return std::size(x);
                                  else
                                    return 1;
                                });
  return std::visit(visitor, get::object(*this));
}

bool parx::json::empty() const noexcept
{
  auto const visitor = overload([](none) -> bool { return true; },
                                [](auto const& x) -> bool {
                                  if
                                    constexpr(has_empty(x)) return std::empty(x);
                                  else
                                    return false;
                                });
  return std::visit(visitor, get::object(*this));
}

auto parx::json::operator[](std::string const& key) -> json&
{
  return get::document(*this).insert(document::value_type(key, {})).first->second;
}

auto parx::json::operator[](std::string const& key) const -> json const&
{
  return get::document(*this).at(key);
}
