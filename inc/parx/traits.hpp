#pragma once
#include <type_traits>

namespace parx
{
  template <typename...>
  struct type_list_t {
  };

  template <typename T>
  using type_t = type_list_t<T>;

  template <typename T>
  constexpr type_list_t<T> type{};

  template <typename... T>
  constexpr type_list_t<T...> type_list{};

  template <template <typename...> typename F, typename... T>
  constexpr bool any_of(type_list_t<T...>)
  {
    return (F<T>::value || ...);
  }

  template <typename T>
  struct is_same_as {
    template <typename U>
    using type = std::is_same<T, U>;
  };

  template <typename T, typename... U>
  constexpr bool contains(type_t<T>, type_list_t<U...> list)
  {
    return any_of<is_same_as<T>::template type>(list);
  }

  template <typename T, typename U, typename = void>
  struct is_comparable : std::false_type {
    constexpr is_comparable(T const&, U const&) {}
  };

  template <typename T, typename U>
  struct is_comparable<T, U, std::void_t<decltype(std::declval<T>() == std::declval<U>())>>
      : std::true_type {
    constexpr is_comparable(T const&, U const&) {}
  };

  template <typename T, typename U>
  is_comparable(T const&, U const&)->is_comparable<T, U>;

  template <typename T, typename = void>
  struct has_size : std::false_type {
    constexpr has_size(T const&) {}
  };

  template <typename T>
  struct has_size<T, std::void_t<decltype(std::size(std::declval<T>()))>> : std::true_type {
    constexpr has_size(T const&) {}
  };

  template <typename T>
  has_size(T const&)->has_size<T>;

  template <typename T, typename = void>
  struct has_empty : std::false_type {
    constexpr has_empty(T const&) {}
  };

  template <typename T>
  struct has_empty<T, std::void_t<decltype(std::empty(std::declval<T>()))>> : std::true_type {
    constexpr has_empty(T const&) {}
  };

  template <typename T>
  has_empty(T const&)->has_empty<T>;
}
