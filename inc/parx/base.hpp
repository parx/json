#pragma once
#include <parx/overload.hpp>

namespace parx
{
  template <typename T>
  auto const base = overload([](auto const& x) -> T const& { return static_cast<T const&>(x); },
                             [](auto& x) -> T& { return static_cast<T&>(x); });
}
