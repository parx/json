#pragma once
#include <variant>
#include <string>
#include <vector>
#include <map>
#include <parx/traits.hpp>
#include <parx/base.hpp>

namespace parx
{
  struct none {
  };
  static constexpr auto const null = none{};

  struct key {
    char const* first;
    char const* last;
  };

  inline namespace string_literals
  {
    parx::key operator"" _key(char const*, std::size_t);
  }

  struct json : std::variant<std::map<std::string, json>,
                             std::vector<json>,
                             none,
                             bool,
                             std::int64_t,
                             double,
                             std::string> {
    using document = std::map<std::string, json>;
    using array = std::vector<json>;
    using object = std::variant<document, array, none, bool, std::int64_t, double, std::string>;
    static constexpr auto alternatives =
        type_list<document, array, none, bool, std::int64_t, double, std::string>;

    template <typename T>
    static constexpr bool is_alternative = contains(type<std::decay_t<T>>, alternatives);

    using size_type = std::size_t;

    using object::object;
    using object::operator=;

    template <
        typename T,
        typename = typename std::enable_if<!is_alternative<T> && std::is_integral<T>::value>::type>
    json(T) noexcept;
    json(char const*);
    json(std::initializer_list<std::pair<parx::key const, json>>);
    json(std::initializer_list<json>);

    size_type size() const noexcept;
    bool empty() const noexcept;

    json& operator[](std::string const&);
    json const& operator[](std::string const&) const;
  };

  template <typename T>
  bool operator==(json const&, T const&);

  namespace get
  {
    auto const object = parx::base<parx::json::object>;
    template <typename T>
    auto const element = [](auto& json) -> decltype(auto) { return std::get<T>(object(json)); };
    auto const document = element<parx::json::document>;
    auto const array = element<parx::json::array>;
    auto const none = element<parx::none>;
    auto const boolean = element<bool>;
    auto const integer = element<std::int64_t>;
    auto const floating = element<double>;
    auto const string = element<std::string>;
  }
}

template <typename T,
          typename = typename std::enable_if<!parx::json::is_alternative<T> &&
                                             std::is_integral<T>::value>::type>
parx::json::json(T t) noexcept : json(static_cast<std::int64_t>(t))
{
}

template <typename T>
bool parx::operator==(json const& json, T const& t)
{
  auto const visitor = [&t](auto const& x) -> bool {
    if
      constexpr(is_comparable<decltype(x), T>::value) return x == t;
    return false;
  };
  return std::visit(visitor, base<json::object>(json));
}
