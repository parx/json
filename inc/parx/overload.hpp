#pragma once

namespace parx
{
  template <typename... F>
  struct overload : F... {
    explicit overload(F... f) : F(std::move(f))... {}
    using F::operator()...;
  };

  template <typename... F>
  overload(F...)->overload<F...>;
}
