#include "inc/parx/json.hpp"
#include "test/catch.hpp"
#include <iterator>

using namespace std::string_literals;
using namespace parx::string_literals;

TEST_CASE("can default create parx::json")
{
  parx::json const json;

  REQUIRE(std::size(json) == 0);
  REQUIRE(std::empty(json));
}

TEST_CASE("can create parx::json document with initializer_list")
{
  parx::json const json = {
      // clang-format off
    {"lorem"_key, 42},
    {"ipsum"_key, true},
    {"dolor"_key, "sit"}
      // clang-format on
  };
  REQUIRE(std::size(json) == 3);
  REQUIRE(!std::empty(json));
}

TEST_CASE("can create parx::json document with setitem")
{
  parx::json json;
  json["lorem"] = 42l;
  json["ipsum"] = true;
  json["dolor"] = "sit"s;
  REQUIRE(std::size(json) == 3);
}

TEST_CASE("can create parx::json array with initializer_list")
{
  parx::json const json = {"lorem", 42, true, parx::null};
  REQUIRE(std::size(json) == 4);
  REQUIRE(!std::empty(json));
}

TEST_CASE("can create parx::json string with string literal")
{
  const char* txt = "hello, world!";
  parx::json const json = txt;
  std::string const str = txt;
  REQUIRE(std::size(json) == std::size(str));
  REQUIRE(!std::empty(json));
  REQUIRE(json == str);
}

TEST_CASE("can create parx::json int with integrals")
{
  {
    parx::json const json = 1;
    REQUIRE(json == 1);
  }
  {
    parx::json const json = short(1);
    REQUIRE(json == 1);
  }
  {
    parx::json const json = 1u;
    REQUIRE(json == 1);
  }
  {
    parx::json const json = 1ull;
    REQUIRE(json == 1);
  }
}
