import common


@common.inherit(common.configure)
def configure(ctx):
    ctx.env.CFLAGS += ['-fsanitize=address']
    ctx.env.CXXFLAGS += ['-fsanitize=address']
    ctx.env.LINKFLAGS += ['-fsanitize=address']
