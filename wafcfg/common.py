from functools import wraps


def inherit(base):
    def decorator(f):
        @wraps(f)
        def wrapper(ctx):
            base(ctx)
            f(ctx)
        return wrapper
    return decorator


flags = [
    '-std=c++14',
    '-Wall',
    '-Wextra',
    '-Werror',
    '-pedantic',
    '-pedantic-errors',
    '-Wshadow',
    '-Wconversion',
    '-Wunused',
    '-Wold-style-cast',
    '-fno-rtti',
]


def configure(ctx):
    ctx.env.CFLAGS = ctx.env.CFLAGS or flags
    ctx.env.CXXFLAGS = ctx.env.CXXFLAGS or flags
    ctx.load('gccdeps')
