import common


@common.inherit(common.configure)
def configure(ctx):
    ctx.env.CFLAGS += ['-fsanitize=undefined']
    ctx.env.CXXFLAGS += ['-fsanitize=undefined']
    ctx.env.LINKFLAGS += ['-fsanitize=undefined']
