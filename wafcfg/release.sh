export CXXFLAGS="-Wall -Wconversion -Werror -Wextra -Wshadow -Wunused -pedantic -pedantic-errors -std=c++17 -O3 -march=native"
export DEFINES="NDEBUG"
export LINKFLAGS="-flto"
